var searchData=
[
  ['sal_5fbool',['sal_Bool',['../a00422.html#a66585e12aa9edc6e11fc7994e1c12286',1,'types.h']]],
  ['sal_5fchar',['sal_Char',['../a00422.html#a50344fc80f122074b8a7e563c5627779',1,'types.h']]],
  ['sal_5fhandle',['sal_Handle',['../a00422.html#aa82fc5b65f0dec2756f6f8f7fa083f80',1,'types.h']]],
  ['sal_5fint8',['sal_Int8',['../a00422.html#ab739fc44e71972374c8f8a7da65a6909',1,'types.h']]],
  ['sal_5fschar',['sal_sChar',['../a00422.html#a771ba56d2a642cf9490e9b4764526629',1,'types.h']]],
  ['sal_5fsequence',['sal_Sequence',['../a00422.html#a2a33b9813432e95f5bf9320e61d777cb',1,'types.h']]],
  ['sal_5fuchar',['sal_uChar',['../a00422.html#a22b3476d565666aaaed72440f01c0171',1,'types.h']]],
  ['sal_5fuint8',['sal_uInt8',['../a00422.html#a462ba47bb2d02c20634cef58fca4ee04',1,'types.h']]],
  ['sal_5funicode',['sal_Unicode',['../a00422.html#aa610619c9a4164b4cd9b6a86351da3cd',1,'types.h']]],
  ['solarguard',['SolarGuard',['../a00465.html#acc0ffa8124b78aaeabd9ac54d5a7e3aa',1,'osl']]]
];
