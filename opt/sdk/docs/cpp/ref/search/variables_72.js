var searchData=
[
  ['rbhelper',['rBHelper',['../a00145.html#a87ecf89272ecccc6d4d8a9cc2ab76938',1,'cppu::OPropertySetHelper']]],
  ['registerinterface',['registerInterface',['../a00031.html#afe7535b25f852380aba2bf2aacc85bf5',1,'_uno_ExtEnvironment']]],
  ['registerproxyinterface',['registerProxyInterface',['../a00031.html#a8de46085fa7923e2f2caf50bce94494b',1,'_uno_ExtEnvironment']]],
  ['release',['release',['../a00007.html#a2d549aa46681771a28d4101209d740e1',1,'_rtl_ModuleCount::release()'],['../a00032.html#ad9261f568570e31f653b8f70b98055ef',1,'_uno_Interface::release()'],['../a00030.html#aa687476bfa6801dd1354626b9cb1d831',1,'_uno_Environment::release()'],['../a00033.html#a1ebaf30ad0463d83e9003fd51a8173df',1,'_uno_Mapping::release()']]],
  ['releaseinterface',['releaseInterface',['../a00031.html#a2605f10f6d18c96c2410ce6c1ccde418',1,'_uno_ExtEnvironment']]],
  ['releaseweak',['releaseWeak',['../a00030.html#aa7ccc383b76226e6698407bed58d8a09',1,'_uno_Environment']]],
  ['reserved',['Reserved',['../a00009.html#a23b0f444733c9da0f6b548d9af2a14eb',1,'_rtl_TextEncodingInfo']]],
  ['revokeinterface',['revokeInterface',['../a00031.html#a1dc5bb1f9b0f97a219beb5ef32ef5c89',1,'_uno_ExtEnvironment']]],
  ['rmutex',['rMutex',['../a00135.html#a4bad032a446dc4fbd790b228a66d15ab',1,'cppu::OBroadcastHelperVar']]]
];
