var searchData=
[
  ['unknown',['Unknown',['../a00092.html#a17884cd168469537e71c44a0813c8d2ba748257fb60af245eba6bd3252d29e051',1,'osl::FileStatus']]],
  ['uno_5fquery',['UNO_QUERY',['../a00274.html#af9a85aee0004c1028a122e7d48850561a4768d25d9d8bff1a951f8dd49edb4c0f',1,'com::sun::star::uno']]],
  ['uno_5fquery_5fthrow',['UNO_QUERY_THROW',['../a00274.html#a73a6783f81e6afa5cf6b7809662856e9ae6161b6f46e058f8cb2d4ab5216d44e9',1,'com::sun::star::uno']]],
  ['uno_5fref_5fno_5facquire',['UNO_REF_NO_ACQUIRE',['../a00274.html#a72b3627936fff38db4ee5943cfcb2326a2bf73c7fe836772b039bc9481febf88b',1,'com::sun::star::uno']]],
  ['uno_5fset_5fthrow',['UNO_SET_THROW',['../a00274.html#afd8938f38e6e9bea9e6ad5eb058ccd26abfdc414638f4f6217e24761c277c09cb',1,'com::sun::star::uno']]],
  ['uno_5ftype_5fno_5facquire',['UNO_TYPE_NO_ACQUIRE',['../a00274.html#a72b6c10d5cb770d942e2156c1b3160f9a00c7c79af29c4f7755c90cfee9d9271d',1,'com::sun::star::uno']]]
];
