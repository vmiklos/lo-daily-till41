/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */
#ifndef __com_sun_star_drawing_Layer_idl__
#define __com_sun_star_drawing_Layer_idl__

#include <com/sun/star/beans/XPropertySet.idl>



 module com {  module sun {  module star {  module drawing {

/** A layer is an entity inside a document which contains shapes.


    <p>There could be zero or more <type>Shape</type>s attached to
    such a layer.

    </p>
    <p>The properties of a <type>Layer</type> instance affect all <type>Shape</type>s
    attached to the Layer.

    </p>

    @see    DrawingDocument
    @see LayerManager
 */
published service Layer
{

    /** The properties in this set affect all <type>Shape</type>s attached to
                this Layer.
     */
    interface com::sun::star::beans::XPropertySet;

    /** The name of a <type>Layer</type> is used to identify the
        <type>Layer</type> in the user interface.
     */
    [property] string Name;

    /** If a <type>Layer</type> is not visible, the objects in this
        <type>Layer</type> are not shown in the user interface.
     */
    [property] boolean IsVisible;

    /** If a <type>Layer</type> is not printable, the objects in
        this <type>Layer</type> are not printed.
     */
    [property] boolean IsPrintable;

    /** If a <type>Layer</type> is locked, the objects in this
        <type>Layer</type> cannot be edited in the user interface.
     */
    [property] boolean IsLocked;

};


}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
