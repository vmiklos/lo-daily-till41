/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#ifndef __com_sun_star_frame_XToolbarControllerFactory_idl__
#define __com_sun_star_frame_XToolbarControllerFactory_idl__

#include <com/sun/star/lang/XMultiComponentFactory.idl>
#include <com/sun/star/frame/XUIControllerRegistration.idl>


module com { module sun { module star { module frame {


/** Provides a unified interface for the new-style PopupMenuControllerFactory service to implement.

    @since LibreOffice 4.1
*/

interface XToolbarControllerFactory
{
    /** this interface provides functions to create new instances of a registered pop-up menu controller.

        <p>
        Use <member scope="com.sun.star.lang">XMultiComponentFactory::createInstanceWithArguments()</member> to create
        a new pop-up menu controller instance. Use the CommandURL as the service specifier.

        This call supports the following arguments provided as <type scope="com::sun::star::beans">PropertyValue</type>:
        <ul>
            <li><b>Frame</b><br>specifies the <type scope="com::sun::star::frame">XFrame</type>
                   instance to which the pop-up menu controller belongs to. This property must be provided to
                   the pop-up menu controller, otherwise it cannot dispatch its internal commands.</li>
            <li><b>ModuleIdentifier</b><br>optional string that specifies in which module context the pop-up menu
                   controller should be created.</li>
        </ul>
        </p>
     */
    interface com::sun::star::lang::XMultiComponentFactory;

    /** provides functions to query for, register and deregister a pop-up menu controller.
     */
    interface com::sun::star::frame::XUIControllerRegistration;
};

}; }; }; };


#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
