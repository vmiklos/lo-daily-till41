/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#ifndef __com_sun_star_datatransfer_clipboard_SystemClipboard_idl__
#define __com_sun_star_datatransfer_clipboard_SystemClipboard_idl__

#include <com/sun/star/awt/XDisplayConnection.idl>
#include <com/sun/star/datatransfer/clipboard/XSystemClipboard.idl>
#include <com/sun/star/script/XInvocation.idl>


module com { module sun { module star { module datatransfer { module clipboard {


/** The system clipboard service builds a bridge to the OS specific clipboard
    interfaces.

    <br/><br/><p><strong>Notes:</strong> The Unix implementation needs to be instantiated with 2 Arguments:
    <ul>
        <li>A <type scope="com::sun::star::awt">XDisplayConnection</type> that provides the
        display to be used.</li>
        <li>A string that names the selection to be used</li>
    </ul>
    It is possible to use clipboards for different selections simultaneously.</p>
*/
published service SystemClipboard : XSystemClipboard
{
    createDefault();

    /** This method is only valid for unix systems (excluding MacOS) */
    createUnix([in] com::sun::star::awt::XDisplayConnection DisplayConnection,
               [in] string ClipboardName,
               [in] com::sun::star::script::XInvocation BmpConverter);
};


}; }; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
